package br.com.sicredi.assembly.service;

import br.com.sicredi.assembly.controller.dto.AssociateRequest;
import br.com.sicredi.assembly.controller.dto.AssociateResponse;
import br.com.sicredi.assembly.controller.dto.AssociateUpdateRequest;
import br.com.sicredi.assembly.domain.Associate;
import br.com.sicredi.assembly.domain.enums.AssociatePermission;
import br.com.sicredi.assembly.repository.AssociateRepository;
import br.com.sicredi.assembly.service.exception.EntityNotFoundException;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class AssociateService {

	@Autowired
	private AssociateRepository associateRepository;

	@Autowired
	private CPFValidatorService cpfValidatorService;

	@Autowired
	private ModelMapper modelMapper;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(AssociateService.class);

	public Associate create(AssociateRequest associateRequest) {
		LOGGER.info("Creating associate...");
		Associate associate = modelMapper.map(associateRequest, Associate.class);
		
		_validateCPF(associate);
		associate.setCreationDate(LocalDateTime.now());
		associate = associateRepository.save(associate);
		
		LOGGER.info("Associate created successfully. Returning value");
		return associate;
	}

	public Associate findById(Integer id) {
		LOGGER.info("Searching associate...");
		
		Optional<Associate> optionalAssociate = associateRepository.findById(id);
		if (optionalAssociate.isEmpty()) {
			LOGGER.error("Associate not found");
			throw new EntityNotFoundException();
		}
		
		LOGGER.info("Associate found. Returning object");
		return optionalAssociate.get();
	}

	public List<AssociateResponse> list() {
		LOGGER.info("Listing associates...");

		List<AssociateResponse> associates = associateRepository.findAll().stream().map(associate -> modelMapper.map(associate, AssociateResponse.class)).collect(Collectors.toList());
		LOGGER.info("Listing done. Returning value");
		return associates;
	}

	public Associate update(AssociateUpdateRequest associateUpdateRequest) {
		LOGGER.info("Updating associate...");
		Associate associate = findById(associateUpdateRequest.getId());
		associate.setCpf(associateUpdateRequest.getCpf());
		associate.setName(associateUpdateRequest.getName());
		associate.setUpdateDate(LocalDateTime.now());
		
		_validateCPF(associate);
		associate = associateRepository.save(associate);
		
		LOGGER.info("Associate updated successfully. Returning value");
		return associate;
	}

	public void delete(Integer id) {
		findById(id);
		LOGGER.info("Deleting associate...");
		associateRepository.deleteById(id);
		LOGGER.info("Associate deleted successfully");
	}
	
	private void _validateCPF(Associate associate) {
		Integer statusCode = cpfValidatorService.callApiValidation(associate.getCpf());
		if (statusCode == HttpStatus.OK.value()) {
			associate.setPermission(AssociatePermission.ABLE_TO_VOTE.getId());
		} else {
			associate.setPermission(AssociatePermission.UNABLE_TO_VOTE.getId());
		}
	}

}
