package br.com.sicredi.assembly.controller;

import br.com.sicredi.assembly.controller.dto.AssemblyRequest;
import br.com.sicredi.assembly.controller.dto.AssemblyResponse;
import br.com.sicredi.assembly.controller.dto.AssemblyUpdateRequest;
import br.com.sicredi.assembly.domain.Assembly;
import br.com.sicredi.assembly.service.AssemblyService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;

@RestController
@RequestMapping(value = "/api/assembly")
public class AssemblyController {
	
	@Autowired
	private AssemblyService assemblyService;
	
	@Autowired
	private ModelMapper modelMapper;
	
	@PostMapping
	public ResponseEntity<AssemblyResponse> create(
			@Valid @RequestBody AssemblyRequest assemblyRequest) {
		Assembly assembly = assemblyService.create(assemblyRequest);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest()
				.path("/{id}").buildAndExpand(assembly.getId()).toUri();
		return ResponseEntity.created(uri)
				.body(modelMapper.map(assembly, AssemblyResponse.class));
	}
	
	@GetMapping(value = "/{id}")
	public ResponseEntity<AssemblyResponse> findById(@PathVariable Integer id) {
		Assembly assembly = assemblyService.findById(id);
		return ResponseEntity.ok(modelMapper.map(assembly, AssemblyResponse.class));
	}
	
	@GetMapping
	public ResponseEntity<List<AssemblyResponse>> list() {
		return ResponseEntity.ok(assemblyService.list());
	}
	
	@PutMapping
	public ResponseEntity<AssemblyResponse> update(
			@Valid @RequestBody AssemblyUpdateRequest assemblyUpdateRequest) {
		Assembly assembly = assemblyService.update(assemblyUpdateRequest);
		return ResponseEntity.ok(modelMapper.map(assembly, AssemblyResponse.class));
	}
	
	@DeleteMapping(value = "/{id}")
	public ResponseEntity<Void> delete(@PathVariable Integer id) {
		assemblyService.delete(id);
		return ResponseEntity.noContent().build();
	}
	
	@PutMapping(value = "/startVoting/{id}")
	public ResponseEntity<AssemblyResponse> startVoting(@PathVariable Integer id) {
		Assembly assembly = assemblyService.startVoting(id);
		return ResponseEntity.ok(modelMapper.map(assembly, AssemblyResponse.class));
	}

}
