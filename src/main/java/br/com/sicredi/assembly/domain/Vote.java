package br.com.sicredi.assembly.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Vote {

    @EmbeddedId
    private VotePK id;

    @Column(nullable = false)
    private Integer voteChoice;

    @Column(nullable = false)
    private LocalDateTime creationDate;

}
