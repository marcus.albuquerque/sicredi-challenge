package br.com.sicredi.assembly.controller;

import br.com.sicredi.assembly.controller.dto.AssociateRequest;
import br.com.sicredi.assembly.controller.dto.AssociateResponse;
import br.com.sicredi.assembly.controller.dto.AssociateUpdateRequest;
import br.com.sicredi.assembly.domain.Associate;
import br.com.sicredi.assembly.service.AssociateService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;

@RestController
@RequestMapping(value = "/api/associate")
public class AssociateController {
	
	@Autowired
	private AssociateService associateService;
	
	@Autowired
	private ModelMapper modelMapper;
	
	@PostMapping
	public ResponseEntity<AssociateResponse> create(
			@Valid @RequestBody AssociateRequest associateRequest) {
		Associate associate = associateService.create(associateRequest);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest()
				.path("/{id}").buildAndExpand(associate.getId()).toUri();
		return ResponseEntity.created(uri)
				.body(modelMapper.map(associate, AssociateResponse.class));
	}
	
	@GetMapping(value = "/{id}")
	public ResponseEntity<AssociateResponse> findById(@PathVariable Integer id) {
		Associate associate = associateService.findById(id);
		return ResponseEntity.ok(modelMapper.map(associate, AssociateResponse.class));
	}
	
	@GetMapping
	public ResponseEntity<List<AssociateResponse>> list() {
		return ResponseEntity.ok(associateService.list());
	}
	
	@PutMapping
	public ResponseEntity<AssociateResponse> update(
			@Valid @RequestBody AssociateUpdateRequest associateUpdateRequest) {
		Associate associate = associateService.update(associateUpdateRequest);
		return ResponseEntity.ok(modelMapper.map(associate, AssociateResponse.class));
	}
	
	@DeleteMapping(value = "/{id}")
	public ResponseEntity<Void> delete(@PathVariable Integer id) {
		associateService.delete(id);
		return ResponseEntity.noContent().build();
	}

}
