package br.com.sicredi.assembly.service;

import br.com.sicredi.assembly.controller.dto.VoteRequest;
import br.com.sicredi.assembly.domain.Assembly;
import br.com.sicredi.assembly.domain.Associate;
import br.com.sicredi.assembly.domain.Vote;
import br.com.sicredi.assembly.domain.VotePK;
import br.com.sicredi.assembly.domain.enums.AssemblyStatus;
import br.com.sicredi.assembly.domain.enums.AssociatePermission;
import br.com.sicredi.assembly.domain.enums.VoteChoice;
import br.com.sicredi.assembly.repository.VoteRepository;
import br.com.sicredi.assembly.service.exception.AssemblyNotStartedException;
import br.com.sicredi.assembly.service.exception.AssociateAlreadyVotedException;
import br.com.sicredi.assembly.service.exception.AssociateUnableToVoteException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Optional;

@Service
public class VoteService {
	
	@Autowired
	private VoteRepository voteRepository;
	
	@Autowired
	private AssemblyService assemblyService;
	
	@Autowired
	private AssociateService associateService;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(VoteService.class);
	
	public Vote vote(VoteRequest voteRequest) {
		LOGGER.info("Computing vote for associate: " + voteRequest.getAssociate()
			+ ", assembly: " + voteRequest.getAssembly());
		Assembly assembly = assemblyService.findById(voteRequest.getAssembly());
		if (assembly.getStatus() != AssemblyStatus.STARTED.getId()) {
			LOGGER.error("Could not compute. Assembly is not started");
			throw new AssemblyNotStartedException();
		}
		
		Associate associate = associateService.findById(voteRequest.getAssociate());
		if (associate.getPermission() == AssociatePermission.UNABLE_TO_VOTE.getId()) {
			LOGGER.error("Could not compute. User is not able to vote");
			throw new AssociateUnableToVoteException();
		}
		
		VoteChoice voteChoice = VoteChoice.toEnum(voteRequest.getVote());
		VotePK votePK = new VotePK(assembly, associate);
		Optional<Vote> optionalVote = voteRepository.findById(votePK);
		if (!optionalVote.isEmpty()) {
			LOGGER.error("Could not compute. User has voted already");
			throw new AssociateAlreadyVotedException();
		}
		Vote vote = new Vote(votePK, voteChoice.getId(), LocalDateTime.now());
		
		vote = voteRepository.save(vote);
		LOGGER.info("Vote computed successfully. Returning value");
		return vote;
	}

}
