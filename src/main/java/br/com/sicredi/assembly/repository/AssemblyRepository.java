package br.com.sicredi.assembly.repository;

import br.com.sicredi.assembly.domain.Assembly;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AssemblyRepository extends JpaRepository<Assembly, Integer> {
	
	Assembly findByTitleContainsIgnoreCase(String title);
	
	Assembly findByStatus(Integer status);

}
