package br.com.sicredi.assembly.repository;

import br.com.sicredi.assembly.domain.Vote;
import br.com.sicredi.assembly.domain.VotePK;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.transaction.Transactional;
import java.util.List;

public interface VoteRepository extends JpaRepository<Vote, VotePK> {
	
	List<Vote> findByIdAssemblyId(Integer assemblyId);
	
	@Transactional
	void deleteByIdAssemblyId(Integer assemblyId);

}
