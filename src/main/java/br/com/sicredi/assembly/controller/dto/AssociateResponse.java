package br.com.sicredi.assembly.controller.dto;

import br.com.sicredi.assembly.domain.enums.AssociatePermission;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AssociateResponse {
	
	private Integer id;
	private String cpf;
	private String name;
	private String permission;
	
	public void setPermission(Integer permission) {
		this.permission = AssociatePermission.toEnum(permission).getDescription();
	}

}
