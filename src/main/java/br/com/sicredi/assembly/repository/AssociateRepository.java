package br.com.sicredi.assembly.repository;

import br.com.sicredi.assembly.domain.Associate;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface AssociateRepository extends JpaRepository<Associate, Integer> {

	
	Optional<Associate> findByCpf(String cpf);

}
