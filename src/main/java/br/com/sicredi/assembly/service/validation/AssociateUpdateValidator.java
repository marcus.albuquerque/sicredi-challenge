package br.com.sicredi.assembly.service.validation;

import br.com.sicredi.assembly.controller.dto.AssociateUpdateRequest;
import br.com.sicredi.assembly.controller.exception.FieldMessage;
import br.com.sicredi.assembly.domain.Associate;
import br.com.sicredi.assembly.repository.AssociateRepository;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class AssociateUpdateValidator implements ConstraintValidator<AssociateUpdate, AssociateUpdateRequest> {

	@Autowired
	private AssociateRepository associateRepository;
	
	@Override
	public void initialize(AssociateUpdate ann) {
	}

	@Override
	public boolean isValid(AssociateUpdateRequest associateUpdateRequest, ConstraintValidatorContext context) {
		List<FieldMessage> list = new ArrayList<>();
		
		Optional<Associate> optionalAssociate = associateRepository.findByCpf(associateUpdateRequest.getCpf());
		
		if (optionalAssociate.isPresent() && !optionalAssociate.get().getId().equals(associateUpdateRequest.getId())) {
			list.add(new FieldMessage("cpf", "This CPF is already registered"));
		}
		
		for (FieldMessage e : list) {
			context.disableDefaultConstraintViolation();
			context.buildConstraintViolationWithTemplate(e.getMessage()).addPropertyNode(e.getFieldName())
					.addConstraintViolation();
		}
		return list.isEmpty();
	}
}
